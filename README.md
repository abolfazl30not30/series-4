# Series 5

## json parser   
Implement classes in sbu.cs.parser.json package.  

* Extract good methods  
* Declare classes where you think they are necessary
* All json that you need to parse have only 5 possible value types
    1. String
    2. number
    3. boolean (true or false)
    4. null
    5. array
* All json that you need to parse have these rules
    1. keys are in double quote (" ")
    2. between key and value there is :
    3. between each key/value pair there is a comma (,)
    4. a json object will always start with { and end with }
    

### Additional score task
* Implement function `getValue` which return the value of a key
   which is not necessarily a String. it can be int, null, boolean 
   or another Json object. for this you probably need to declare classes
   for values.
   
* Write a json parser which can parse json that have a key mapped to
   another json.  
   
   
### Reference
For further information check this [Link](https://www.udacity.com/blog/2021/02/javascript-json-parse.html#:~:text=JSON%20parsing%20is%20the%20process,by%20using%20the%20method%20JSON.)



## DOM
Implement classes in sbu.cs.parser.html package.


* Extract good methods
* Declare classes where you think they are needed
* In all given HTML strings to parse these attributes are guaranteed.
    1. All tags which starts will end eventually. There is no tag which it has been opened and has never been closed.
    2. Tags may have attributes and all attributes are in `key / value` pairs

    
#### Additional score task
* Implement function `toHTMLString` which gets a DOM object (Node root) and 
    returns a string that is the HTML representation of DOM object (opposite of parse)


---
## String trim function
```java
String str = "  salam ";
System.out.println(str.trim());     // will print 'salam'
```  
For more information, feel free to check out this [Link](https://www.geeksforgeeks.org/java-string-trim-method-example/).
