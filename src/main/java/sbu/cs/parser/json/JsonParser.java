package sbu.cs.parser.json;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JsonParser {

    /*
    * this function will get a String and returns a Json object
     */

    public static Json parse(String data) {
        Json json = new Json();
        Pattern pattern = Pattern.compile("(?:\\\"|\\')(?<key>[\\w\\d]+)(?:\\\"|\\')(?:\\:\\s*)(?:\\\"|\\')?(?<value>(\\[\\s*(\\s*(\\s*\\w*\\,)|\\s*\\w)+\\s*\\])|[\\w-]*)?");
        Matcher matcher = pattern.matcher(data);
        while (matcher.find()){
            String key = matcher.group("key");
            String value = matcher.group("value");
            json.addKeyAndValue(key,value);
        }
        return json;
    }
    /*
    * this function is the opposite of above function. implementing this has no score and
    * is only for those who want to practice more.
     */
    public static String toJsonString(Json data) {
        return null;
    }
}
