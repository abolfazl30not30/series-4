package sbu.cs.parser.json;

import jdk.dynalink.linker.LinkerServices;

import java.util.ArrayList;
import java.util.List;

public class Json implements JsonInterface {

    private  List<KeysAndValues> listOfkeysAndValues;

    public Json(){
        listOfkeysAndValues = new ArrayList<KeysAndValues>();
    }
    @Override
    public String getStringValue(String key){
        for(KeysAndValues kandv : listOfkeysAndValues){
            if(key.equals(kandv.getKey())){
                return kandv.getValue();
            }
        }
        return null;
    }
    public void addKeyAndValue(String key,String value){
        KeysAndValues kandv = new KeysAndValues(key,value);
        listOfkeysAndValues.add(kandv);
    }

}
