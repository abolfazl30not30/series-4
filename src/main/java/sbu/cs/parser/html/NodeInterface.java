package sbu.cs.parser.html;

import java.util.List;

public interface NodeInterface {

    public Node getChild(int number);
    public String getStringInside();
    public String getAttributeValue(String key);
}
